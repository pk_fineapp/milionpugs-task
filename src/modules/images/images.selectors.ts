import { createSelector } from '@reduxjs/toolkit';

import { GlobalState } from '../../config/reducers';

const selectImagesDomain = (state: GlobalState) => state.images;

export const selectStatus = createSelector(selectImagesDomain, (state) => state.status);

export const selectImage = createSelector(selectImagesDomain, (state) => state.image);

export const selectSaved = createSelector(selectImagesDomain, (state) => state.saved);

export const selectViewed = createSelector(selectImagesDomain, (state) => state.viewed);
