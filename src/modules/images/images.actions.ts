import { actionCreator } from '../helpers';
import { ImageType, ViewedImageType } from './images.types';

const { createAction } = actionCreator('IMAGES');

export const fetchImage = createAction<void>('FETCH_IMAGE');

export const fetchImageSuccess = createAction<ImageType>('FETCH_IMAGE_SUCCESS');

export const fetchImageFailure = createAction<void>('FETCH_IMAGE_FAILURE');

export const saveImage = createAction<ImageType>('SAVE_IMAGE');

export const fetchViewedImages = createAction<void>('FETCH_VIEWED_IMAGES');

export const fetchViewedImagesSuccess = createAction<ViewedImageType[]>('FETCH_VIEWED_IMAGES_SUCCESS');
