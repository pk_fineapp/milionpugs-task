export interface ImagesState {
  status: ImagesFetchStatus;
  image: ImageType | null;
  saved: ImageType[];
  viewed: ViewedImageType[];
}

export enum ImagesFetchStatus {
  IDLE = 'idle',
  LOADING = 'loading',
  SUCCESS = 'success',
  FAILURE = 'failure',
}

export interface ImageType {
  date: string,
  title: string,
  url: string,
  hdurl?: string,
  media_type?: string,
  copyright: string
  service_version?: string,
  explanation?: string,
}

export interface ViewedImageType {
  title: string,
  url: string,
}
