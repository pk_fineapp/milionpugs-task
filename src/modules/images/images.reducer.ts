import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import * as imagesActions from './images.actions';
import { ImagesState, ImagesFetchStatus, ImageType, ViewedImageType } from './images.types';

export const INITIAL_STATE: ImagesState = {
  status: ImagesFetchStatus.IDLE,
  image: null,
  saved: [],
  viewed: []
};

const handleFetchImage = (state: ImagesState) => {
  state.status = ImagesFetchStatus.LOADING;
};

const handleFetchImagesSuccess = (state: ImagesState, { payload: image }: PayloadAction<ImageType>) => {
  return {
    ...state,
    image,
    status: ImagesFetchStatus.SUCCESS,
    viewed: [...state.viewed, { title: image.title, url: image.url }],
  };
};

const handleFetchImagesFailure = (state: ImagesState) => {
  state.status = ImagesFetchStatus.FAILURE;
};

const handleSaveImage = (state: ImagesState, { payload: image }: PayloadAction<ImageType>) => {
  state.saved = [...state.saved, image];
};

const handleFetchViewedImagesSuccess = (state: ImagesState, { payload: viewedImages }: PayloadAction<ViewedImageType[]>) => {
  return {
    ...state,
    viewed: viewedImages,
  };
};

export const reducer = createReducer(INITIAL_STATE, (builder) => {
  builder.addCase(imagesActions.fetchImage, handleFetchImage);
  builder.addCase(imagesActions.fetchImageSuccess, handleFetchImagesSuccess);
  builder.addCase(imagesActions.fetchImageFailure, handleFetchImagesFailure);
  builder.addCase(imagesActions.saveImage, handleSaveImage);
  builder.addCase(imagesActions.fetchViewedImagesSuccess, handleFetchViewedImagesSuccess);
});
