import { all, put, takeLatest, select } from 'redux-saga/effects';

import { reportError } from '../../shared/utils/reportError';
import { api, nasaApi } from '../../shared/services/api';
import { db } from '../../shared/services/firebase';
import * as imagesActions from './images.actions';
import { ImageType, ViewedImageType } from './images.types';
import { FIRESTORE_COLLECTION_NAME } from './images.constants';
import { imagesSelectors } from './index';

export interface ReduxAction<T> {
  type: string;
  payload: T;
}

function* fetchImage() {
  try {
    const viewedImages = yield select(imagesSelectors.selectViewed);
    const { data } = yield api.get(`${nasaApi.baseUrl}/?api_key=${nasaApi.apiKey}&count=1`);
    const image = data[0];
    const isPreviouslyViewed = viewedImages.some((viewedImage: ViewedImageType) => {
      return viewedImage.title === image.title && viewedImage.url === image.url;
    });

    if (isPreviouslyViewed) {
      yield put(imagesActions.fetchImage());
    } else {
      yield put(imagesActions.fetchImageSuccess(image));
    }
  } catch (error) {
    reportError(error);
    yield put(imagesActions.fetchImageFailure());
  }
}

function* saveAsViewed({ payload: image }: ReduxAction<ImageType>) {
  try {
    yield db().collection(FIRESTORE_COLLECTION_NAME).doc().set({ title: image.title, url: image.url })
  } catch (error) {
    reportError(error);
    yield put(imagesActions.fetchImageFailure());
  }
}

function* fetchViewedImages() {
  try {
    const viewedImages: ViewedImageType[] = [];
    yield db().collection(FIRESTORE_COLLECTION_NAME)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(function(doc) {
          viewedImages.push(doc.data() as ViewedImageType)
        });
      });

    yield put(imagesActions.fetchViewedImagesSuccess(viewedImages))
  } catch (error) {
    reportError(error);
  }
}

export function* watchImages() {
  yield all([takeLatest(imagesActions.fetchImage, fetchImage)]);
  yield all([takeLatest(imagesActions.fetchImageSuccess, saveAsViewed)]);
  yield all([takeLatest(imagesActions.fetchViewedImages, fetchViewedImages)]);
  yield all([takeLatest(imagesActions.fetchViewedImagesSuccess, fetchImage)]);
}
