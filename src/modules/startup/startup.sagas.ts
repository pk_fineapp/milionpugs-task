import { all, put, takeLatest } from 'redux-saga/effects';
import { reportError } from '../../shared/utils/reportError';
import { imagesActions } from '../images';
import { startupActions } from '.';

// eslint-disable-next-line @typescript-eslint/no-empty-function
export function* handleStartup() {
  try {
    yield put(imagesActions.fetchViewedImages());
  } catch (error) {
    reportError(error);
  }
}

export function* watchStartup() {
  yield all([takeLatest(startupActions.startup, handleStartup)]);
}
