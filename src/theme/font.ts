export const fontFamily = {
  primary: 'Helvetica',
};

export const fontWeight = {
  bold: 'bold',
};
