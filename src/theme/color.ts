export const white = '#ffffff';
export const black = '#000000';

export const border = '#80809B';
export const disabled = '#DDDDE0';
export const primary = '#434343';
export const secondary = '#434343';
export const error = '#fa7265';
