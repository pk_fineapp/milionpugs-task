import { combineReducers } from 'redux';

import { reducer as localesReducer } from '../modules/locales/locales.reducer';
import { LocalesState } from '../modules/locales/locales.types';
import { reducer as startupReducer } from '../modules/startup/startup.reducer';
import { StartupState } from '../modules/startup/startup.types';
import { reducer as usersReducer } from '../modules/users/users.reducer';
import { UsersState } from '../modules/users/users.types';
import { reducer as imagesReducer } from '../modules/images/images.reducer';
import { ImagesState } from '../modules/images/images.types';
//<-- IMPORT MODULE REDUCER -->

export type GlobalState = {
  locales: LocalesState;
  startup: StartupState;
  users: UsersState;
  images: ImagesState;
  //<-- INJECT MODULE STATE TYPE -->
};

export default function createReducer() {
  return combineReducers({
    locales: localesReducer,
    startup: startupReducer,
    users: usersReducer,
    images: imagesReducer,
    //<-- INJECT MODULE REDUCER -->
  });
}
