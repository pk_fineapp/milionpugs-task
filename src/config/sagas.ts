import { all, fork } from 'redux-saga/effects';

import { watchStartup } from '../modules/startup/startup.sagas';
import { watchUsers } from '../modules/users/users.sagas';
import { watchImages } from '../modules/images/images.sagas';
//<-- IMPORT MODULE SAGA -->

export default function* rootSaga() {
  yield all([
    fork(watchStartup),
    fork(watchUsers),
    fork(watchImages),
    //<-- INJECT MODULE SAGA -->
  ]);
}
