import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { startupActions } from '../../modules/startup';
import { initializeFirestore } from '../../shared/services/firebase';
import initializeFontFace from '../../theme/initializeFontFace';

export const useStartup = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    initializeFirestore();
    dispatch(startupActions.startup());
    initializeFontFace();
  }, [dispatch]);
};
