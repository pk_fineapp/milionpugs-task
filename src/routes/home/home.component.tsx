import React from 'react';
import { Helmet } from 'react-helmet-async';
import { useDispatch, useSelector } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';

import { imagesActions, imagesSelectors } from '../../modules/images';
import { renderWhenTrueOtherwise } from '../../shared/utils/rendering';
import { ImagesFetchStatus, ImageType } from '../../modules/images/images.types';
import { Button } from '../../shared/components/button';
import { ButtonVariant } from '../../shared/components/button/button.types';
import {
  Actions,
  Container,
  Copyright,
  Date,
  Image,
  ImageContainer,
  ImageDetails,
  ImageTitle,
  ImageWrapper,
  Loader,
  Placeholder,
} from './home.styles';
import messages from './home.messages';

export const Home = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const image = useSelector(imagesSelectors.selectImage);
  const status = useSelector(imagesSelectors.selectStatus);
  const savedImages = useSelector(imagesSelectors.selectSaved);
  const isAlreadySaved = savedImages.some((savedImage: ImageType) => savedImage === image);

  const handleSaveImage = () => dispatch(imagesActions.saveImage(image!));

  const handleNextImage = () => dispatch(imagesActions.fetchImage());

  const renderImage = renderWhenTrueOtherwise(() => (
    <>
      <ImageWrapper>
        <Placeholder />
        <Image src={image!.url} alt="NASA Astronomy Picture of the Day" />
      </ImageWrapper>
      <ImageDetails>
        <ImageTitle>{image?.title}</ImageTitle>
        <Copyright>{image?.copyright}</Copyright>
        <Date>{image?.date}</Date>
      </ImageDetails>
    </>
  ), () => (
    <>
      <ImageWrapper>
        <Placeholder />
        <Loader />
      </ImageWrapper>
      <ImageDetails />
    </>
  ))(!!image && status === ImagesFetchStatus.SUCCESS);

  return (
    <Container>
      <Helmet
        title={intl.formatMessage({
          defaultMessage: 'Homepage',
        })}
      />
      <ImageContainer>
        {renderImage}
      </ImageContainer>
      <Actions>
        <Button type="button" onClick={handleSaveImage} disabled={isAlreadySaved} variant={ButtonVariant.SECONDARY}>
          {isAlreadySaved ? <FormattedMessage {...messages.saved} /> : <FormattedMessage {...messages.save} />}
        </Button>
        <Button type="button" onClick={handleNextImage}>
          <FormattedMessage {...messages.next} />
        </Button>
      </Actions>
    </Container>
  );
};
