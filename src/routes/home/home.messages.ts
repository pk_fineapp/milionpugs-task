/* eslint-disable max-len */
import { defineMessages } from 'react-intl';

const scope = 'home';

export default defineMessages({
  save: {
    id: `${scope}.save`,
    defaultMessage: 'Zapisz',
  },
  saved: {
    id: `${scope}.saved`,
    defaultMessage: 'Zapisano',
  },
  next: {
    id: `${scope}.next`,
    defaultMessage: 'Następne',
  },
});
