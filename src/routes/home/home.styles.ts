import styled from 'styled-components';
import { Breakpoint, media } from '../../theme/media';

export const Container = styled.div`
  height: 100%;
  min-height: 100vh;
  width: 100%;
  padding: 120px 20px 20px;
  background-color: #f3f3f3;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  
  ${media(Breakpoint.DESKTOP)`
    padding: 120px 60px 20px;
  `};
`;

export const ImageContainer = styled.div`
  width: 500px;
  height: 500px;
  max-width: 100%;
  display: flex;
  flex-direction: column;
`;

export const ImageWrapper = styled.div`
  height: 80%;
  width: 100%;
  position: relative;
  border-radius: 8px;
  overflow: hidden;
`;

export const Image = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const ImageDetails = styled.div`
  width: 100%;
  padding-top: 24px;
`;

export const Actions = styled.div`
  width: 500px;
  max-width: 90%;
  margin-top: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  
  button {
    width: 300px;
    
    &:first-of-type {
      margin-right: 20px;
    }
  }
`;

export const Placeholder = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #eee;
  overflow: hidden;
  
  &:after {
    position: absolute;
    display: block;
    content: '';
    left: -150px;
    top: 0;
    height: 100%;
    width: 150px;
    background: linear-gradient(to right, transparent 0%, #E8E8E8 50%, transparent 100%);
    animation: load 1.5s cubic-bezier(0.4, 0.0, 0.2, 1) infinite;
  }
  
  @keyframes load {
    from {
        left: -150px;
    }
    to   {
        left: 100%;
    }
  }
`;

export const Loader = styled.div`
  position: absolute;
  top: calc(50% - 20px);
  left: calc(50% - 20px);
  border: 6px solid #f3f3f3;
  border-radius: 50%;
  border-top: 6px solid #434343;
  width: 40px;
  height: 40px;
  animation: spin 1s linear infinite;
  
  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
`;

export const ImageTitle = styled.p`
  font-weight: bold;
  padding: 0;
  margin: 0;
`;

export const Copyright = styled.p`
  opacity: 0.5;
  padding: 0;
  margin: 10px 0 4px;
  font-size: 0.8em;
`;

export const Date = styled.p`
  font-style: italic;
  opacity: 0.5;
  padding: 0;
  margin: 0;
  font-size: 0.6em;
`;
