import React from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import { isEmpty } from 'ramda';

import { imagesSelectors } from '../../modules/images';
import { ImageType } from '../../modules/images/images.types';
import { Copyright, ImageTitle, Date } from '../home/home.styles';
import { Container, Gallery, GalleryItem, Details, Image } from './saved.styles';
import messages from './saved.messages';

export const Saved = () => {
  const intl = useIntl();
  const savedImages = useSelector(imagesSelectors.selectSaved);

  return (
    <Container>
      <Helmet
        title={intl.formatMessage({
          defaultMessage: 'Zapisane',
        })}
      />
      <h2>
        <FormattedMessage {...messages.title} />
      </h2>
      {isEmpty(savedImages) ?
        <p>
          <FormattedMessage {...messages.noImages} />
        </p> :
        <p>
          <FormattedMessage {...messages.imagesCount} values={{ count: savedImages.length }}/>
        </p>
      }
      {!isEmpty(savedImages) &&
        <Gallery>
          {savedImages.map(({ url, title, copyright, date }: ImageType) => (
            <GalleryItem key={title}>
              <Image src={url} alt={title} />
              <Details>
                <ImageTitle>{title}</ImageTitle>
                <Copyright>{copyright}</Copyright>
                <Date>{date}</Date>
              </Details>
            </GalleryItem>
          ))}
        </Gallery>
      }

    </Container>
  );
};
