import styled from 'styled-components';
import { Breakpoint, media } from '../../theme/media';

export const Container = styled.div`
  height: 100vh;
  width: 100%;
  padding: 80px 20px 0;
  background-color: #f3f3f3;
  display: flex;
  flex-flow: column nowrap;
  overflow-y: hidden;
  
  ${media(Breakpoint.DESKTOP)`
    padding: 120px 60px 20px;
  `};
`;

export const Gallery = styled.div`
  height: 100%;
  overflow-y: auto;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  grid-gap: 8px;
  
  ${media(Breakpoint.DESKTOP)`
    grid-template-columns: repeat(2, minmax(0, 1fr));
    grid-gap: 15px;
  `};
`;

export const GalleryItem = styled.div`
  max-height: 100%;
  width: 100%;
  display: flex;
  flex-flow: column nowrap;
`;

export const Image = styled.img`
  height: 200px;
  object-fit: cover;
  background: linear-gradient(30deg, transparent 0%, #E8E8E8 50%, transparent 100%);
  border-radius: 8px;
`;

export const Details = styled.div`
  display: flex;
  flex-flow: column nowrap;
  padding: 16px 0;
`;
