/* eslint-disable max-len */
import { defineMessages } from 'react-intl';

const scope = 'home';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Zapisane zdjęcia',
  },
  noImages: {
    id: `${scope}.noImages`,
    defaultMessage: 'Nie masz żadnych zapisanych zdjęć',
  },
  imagesCount: {
    id: `${scope}.imagesCount`,
    defaultMessage: 'Liczba zapisanych zdjęć: {count}',
  },
});
