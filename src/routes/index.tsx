import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { IntlProvider } from 'react-intl';

import { DEFAULT_LOCALE, translationMessages } from '../i18n';
import { asyncComponent } from '../shared/utils/asyncComponent';
import { AppComponent as App } from './app.component';
import { ROUTES } from './app.constants';
//<-- IMPORT ROUTE -->

const Home = asyncComponent(() => import('./home'), 'Home');
const Saved = asyncComponent(() => import('./saved'), 'Saved');
const NotFound = asyncComponent(() => import('./notFound'), 'NotFound');

const MatchedLanguageComponent = () => {
  return (
    <App>
      <Switch>
        <Route exact path={ROUTES.home}>
          <Home />
        </Route>

        <Route exact path={ROUTES.saved}>
          <Saved />
        </Route>
        {/* <-- INJECT ROUTE --> */}

        <Route>
          <NotFound />
        </Route>
      </Switch>
    </App>
  );
};

export default () => {
  return (
    <Switch>
      <MatchedLanguageComponent />

      <IntlProvider locale={DEFAULT_LOCALE} messages={translationMessages[DEFAULT_LOCALE]}>
        <Route>
          <NotFound />
        </Route>
      </IntlProvider>
    </Switch>
  );
};
