import axios from 'axios';

const baseUrl = process.env.REACT_APP_BASE_API_URL;
const nasaBaseUrl = process.env.REACT_APP_NASA_APOD_BASE_API_URL;
const nasaApiKey = process.env.REACT_APP_NASA_APOD_API_KEY;

if (!baseUrl) {
  throw new Error('REACT_APP_BASE_API_URL env is missing');
}

export const api = axios.create({
  baseURL: baseUrl,
});


export const nasaApi = {
  baseUrl: nasaBaseUrl,
  apiKey: nasaApiKey,
}


