import firebase from 'firebase';
import 'firebase/firestore';

let appInitialised = false;

export const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
};

export const initializeFirestore = () => {
  if (appInitialised) {
    return false;
  }

  firebase.initializeApp(firebaseConfig);
  return (appInitialised = true);
};

export const db = () => firebase.firestore();
