/* eslint-disable max-len */
import { defineMessages } from 'react-intl';

const scope = 'navigation';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Millionpugs Task',
  },
  homeLink: {
    id: `${scope}.homeLink`,
    defaultMessage: 'Strona główna',
  },
  savedLink: {
    id: `${scope}.savedLink`,
    defaultMessage: 'Zapisane',
  },
});
