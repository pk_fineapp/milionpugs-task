import React from 'react';

import { FormattedMessage } from 'react-intl';
import { ROUTES } from '../../../routes/app.constants';
import { Container, Nav, Link, MainTitle} from './navigation.styles';
import messages from './navigation.messages';

export const Navigation = () => {
  return (
    <Container>
      <MainTitle>
        <FormattedMessage {...messages.title} />
      </MainTitle>
      <Nav>
        <Link to={ROUTES.home}>
          <FormattedMessage {...messages.homeLink} />
        </Link>
        <Link to={ROUTES.saved}>
          <FormattedMessage {...messages.savedLink} />
        </Link>
      </Nav>
    </Container>
  );
};
