import styled from 'styled-components';
import { Link as LinkItem } from 'react-router-dom';
import { Breakpoint, media } from '../../../theme/media';

export const Container = styled.header`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 88px;
  padding: 0 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #f3f3f3;
  
  ${media(Breakpoint.DESKTOP)`
    padding: 0 60px;
  `};
`;

export const Nav = styled.nav`
  display: flex;
  align-items: center;
`;

export const Link = styled(LinkItem)`
  margin-right: 20px;
  text-transform: uppercase;
  text-decoration: none;
  font-size: 0.8em;
  color: #434343;
   
  &:hover {
    opacity: 0.8;
  }
  
  &:last-of-type {
    margin-right: 0;
  }
`;

export const MainTitle = styled.h1`
  font-size: 1em;
`;
