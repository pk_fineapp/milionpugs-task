import styled, { css, ThemeProps } from 'styled-components';
import theme from 'styled-theming';

import { border, color, size } from '../../../theme';
import { ButtonVariant, ButtonTheme } from './button.types';

const disabledButtonStyle = css`
  background: ${color.disabled};
  opacity: 0.5;

  color: ${theme('variant', {
    [ButtonVariant.PRIMARY]: color.primary,
    [ButtonVariant.SECONDARY]: color.secondary,
  })};
  
  &:hover,
  &:active {
    background: ${color.disabled};
    cursor: default;
  }
`;

export const Container = styled.button<ThemeProps<ButtonTheme>>`
  padding: ${size.contentVerticalPadding}px ${size.contentHorizontalPadding}px;
  border: ${border.regular};
  border-radius: 8px;
  cursor: pointer;

  color: ${theme('variant', {
    [ButtonVariant.PRIMARY]: color.primary,
    [ButtonVariant.SECONDARY]: color.secondary,
  })};
  
  &:hover,
  &:active {
    background-color: white;
  }

  ${theme('isDisabled', {
    true: disabledButtonStyle,
  })};
`;
